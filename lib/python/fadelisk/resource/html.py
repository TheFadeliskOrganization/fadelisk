from mako import exceptions
from twisted.web import resource

from .bad_request import BadRequestResource


class HTMLResource(resource.Resource):
    isLeaf = True
    allowedMethods = ("GET", "POST", "HEAD")

    def __init__(self, path, registry, site):
        self.path = path
        self.registry = registry
        self.site = site

        super().__init__()

    def render(self, request):
        if request.method.decode() not in self.__class__.allowedMethods:
            return BadRequestResource(self.site).render(request).encode()

        this_path = request.path.decode()
        if "//" in this_path:
            request.setHeader("location", this_path.replace("//", "/"))
            request.setResponseCode(301)
            return ""

        try:
            content = self.site.render_request(request)
        except Exception as exc:
            self.site.app.log.error(exc)
            request.setResponseCode(500)
            if self.site.conf.get("debug"):
                return exceptions.html_error_template().render()
            else:
                return self.site.internal_server_error_resource.render(request)

        payload = self.site.render_context.get_request_data()["payload"]
        if payload is not None:
            return payload

        return content
