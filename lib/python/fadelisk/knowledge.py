from os.path import exists, join
import yaml

try:
    from yaml import CLoader as YAMLLoader
except ImportError:
    from yaml import Loader as YAMLLoader


# pylint: disable=multiple-statements
class KnowledgeNotFound(Exception):
    pass


# pylint: enable=multiple-statements


class Knowledge:
    def __init__(self, site):
        self.site = site

        possible_paths = [
            site.rel_path("lib", "knowledge"),
            site.rel_path("knowledge"),
            *self.site.app.conf["knowledge_paths"],
        ]
        self.paths = []
        for dir_ in possible_paths:
            if exists(dir_):
                self.paths.append(dir_)

    def get(self, name):
        for dir_ in self.paths:
            filename = join(dir_, name) + ".yaml"
            if not exists(filename):
                continue
            with open(filename, encoding="utf-8") as file_:
                return yaml.load(file_, Loader=YAMLLoader)
        raise KnowledgeNotFound(f"There is no knowledge of {name}")
