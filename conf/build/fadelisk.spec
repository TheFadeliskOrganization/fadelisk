
%define usrsbindir /usr/sbin
%define systemd_system /usr/lib/systemd/system
%define nginx_confd %{_sysconfdir}/nginx/conf.d
%define www_dir /srv/www
%define www_sites_dir %{www_dir}/sites

Name: fadelisk
Summary: A webserver where all HTML documents are templates.
Vendor: The Fadelisk Organization
URL: http://www.fadelisk.org/
License: GPLv3

Version: 0.9
Release: 1
SOURCE0 : %{name}-%{version}.tar.gz
Packager: patrick@fadelisk.org
Provides: Fadelisk
Group: Servers/Web Services
Requires: PyYAML python-mako python-twisted-web

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
%{summary}

%prep
%setup -q

%build
%define debug_package %{nil}

%install
# Executable
install -d  %{buildroot}%{usrsbindir}
ln -s %{python_sitelib}/%{name}/main.py %{buildroot}%{usrsbindir}/%{name}

# Python package
install -d %{buildroot}%{python_sitelib}
cp -a lib/python/%{name} %{buildroot}%{python_sitelib}

# Application libraries and templates
%define fadelisk_lib /usr/lib/%{name}
install -d %{buildroot}%{fadelisk_lib}/packages
cp -a lib/templates %{buildroot}%{fadelisk_lib}
cp -a lib/knowledge %{buildroot}%{fadelisk_lib}

# Example configuration
install -d %{buildroot}%{_sysconfdir}/%{name}
install -m 644 conf/example.%{name}.yaml %{buildroot}%{_sysconfdir}/%{name}

# Service file for systemd
install -d %{buildroot}%{systemd_system}
mv conf/system/%{name}.service conf/system/%{name}-local.service
sed 's/\/usr\/local/\/usr/g' conf/system/%{name}-local.service \
    >conf/system/%{name}.service
install -m 644 conf/system/%{name}.service %{buildroot}%{systemd_system}

# Site configuration loader for nginx
install -d %{buildroot}%{nginx_confd}
install -m 644 conf/system/nginx-fhs4.conf %{buildroot}%{nginx_confd}/fhs4.conf

# Package docs
install -d %{buildroot}%{_defaultdocdir}
install -m 644 README.md %{buildroot}%{_defaultdocdir}
install -d %{buildroot}%{_defaultlicensedir}
install -m 644 LICENSE %{buildroot}%{_defaultlicensedir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/%{name}/example.%{name}.yaml
%{systemd_system}/%{name}.service
%{nginx_confd}/fhs4.conf
%{usrsbindir}/%{name}
%{python_sitelib}/%{name}
/usr/lib/%{name}/templates
/usr/lib/%{name}/packages
/usr/lib/%{name}/knowledge
%{_defaultdocdir}/README.md
%{_defaultlicensedir}/LICENSE

%pre
test "$1" == 1 && semanage port -a -t http_port_t -p tcp 1066
mkdir -p /srv/www/sites
semanage fcontext -a -t httpd_sys_content_t /srv/www/sites
restorecon -R -v /srv/www/sites
systemctl daemon-reload

%postun
systemctl daemon-reload
test "$1" == 0 && semanage port -d -t http_port_t -p tcp 1066
