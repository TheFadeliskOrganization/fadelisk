from twisted.web import http, resource


class SiteNotFoundResource(resource.ErrorPage):
    def __init__(self, app):
        self.app = app

        super().__init__(
            http.NOT_FOUND,
            "No Such Site",
            "Your request does not correspond to a known site.",
        )

    def render(self, request):
        self.app.log.warning(
            "Site not found for %s" % request.getHeader("host")
            or "(unknown host)"
        )
        return resource.ErrorPage.render(self, request)
