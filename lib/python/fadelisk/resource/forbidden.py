from twisted.web import http

from .error import ErrorResource


class ForbiddenResource(ErrorResource):
    def __init__(
        self,
        site,
        path=None,
        response_code=http.FORBIDDEN,
        title="Forbidden",
        message="You do not have access to the requested document.",
    ):
        _path = path or site.conf.get(
            "error_page_403", "/errors/403_forbidden.html"
        )
        super().__init__(site, _path, response_code, title, message)
