import os
from os.path import abspath, dirname, exists, isdir, join
from importlib import import_module
from signal import signal, SIG_DFL, SIG_IGN, SIGHUP, SIGINT, SIGTERM, SIGQUIT
import platform
import yaml
import saturnv
from twisted.web import server, vhost

from .site import FadeliskSite
from .resource import SiteNotFoundResource

# Mapping of fast reactor implementations and the systems that support them
fast_reactor_implementations = {
    "epollreactor": ["Linux"],
    "kqueuereactor": ["FreeBSD", "OpenBSD"],
    "pollreactor": ["Darwin"],
}

# Import a faster reactor, if one is available
for implementation, target_systems in fast_reactor_implementations.items():
    if platform.system() in target_systems:
        import_module(f"twisted.internet.{implementation}").install()
        break
# pylint: disable=wrong-import-order, wrong-import-position
from twisted.internet import reactor

# pylint: enable=wrong-import-order, wrong-import-position


class FadeliskServer(server.Site):
    def __init__(self, app):
        self.app = app

        self.sites = []
        self.vhost = vhost.NameVirtualHost()
        super().__init__(self.vhost)

        self.build_global_template_paths()
        self.build_global_knowledge_paths()
        self.vhost.default = SiteNotFoundResource(self.app)
        self.gather_all_sites()

        self.server_header = app.conf.get("server_header", "fadelisk/1.0")

        reactor.listenTCP(  # pylint: disable=no-member
            int(app.conf["listen_port"]),
            self,
            interface=app.conf["bind_address"],
        )

    def catch_interrupts(self, catch):
        # Stop on termination signals
        for sig in (SIGINT, SIGTERM, SIGQUIT):
            signal(sig, self.stop_on_signal if catch else SIG_DFL)
        # Ignore SIGHUP
        signal(SIGHUP, SIG_IGN if catch else SIG_DFL)

    def run(self):
        self.catch_interrupts(True)
        reactor.run()  # pylint: disable=no-member

    def stop(self):  # pylint: disable=no-self-use
        self.catch_interrupts(False)
        reactor.stop()  # pylint: disable=no-member

    def stop_on_signal(self, signum, frame):  # pylint: disable=unused-argument
        self.app.log.info(f"Stopping reactor on signal {signum}")
        self.stop()

    def build_global_template_paths(self):
        lib_dir = self.app.conf["lib_dir"]
        template_paths = self.app.conf.get("extra_template_directories", [])
        template_paths.extend(
            [
                join(lib_dir, "templates"),
                join(lib_dir, "packages"),
                "/usr/local/lib/fadelisk/templates",
                "/usr/local/lib/fadelisk/packages",
                "/usr/lib/fadelisk/templates",
                "/usr/lib/fadelisk/packages",
            ]
        )
        self.app.conf["template_paths"] = template_paths

    def build_global_knowledge_paths(self):
        # Since fadelisk's self-contained layout places modules in lib/python,
        # the true lib dir is one level higher
        self.app.conf["knowledge_paths"] = [
            join(dirname(self.app.conf["lib_dir"]), "knowledge"),
            "/usr/local/lib/fadelisk/knowledge",
            "/usr/lib/fadelisk/knowledge",
        ]

    def gather_all_sites(self):
        for collection in self.app.conf["site_collections"]:
            if exists(collection):
                self.gather_collection(collection)
        if not self.sites:
            self.app.log.warning("No sites could be loaded.")

    def gather_collection(self, collection):
        for item in os.listdir(collection):
            self.enroll_site(item, collection)

    def in_sites(self, fqdn):
        return fqdn in [s.fqdn for s in self.sites]

    def in_site_aliases(self, site, fqdn):  # pylint: disable=no-self-use
        return fqdn in site.aliases

    def sites_with_alias(self, fqdn):
        return [
            site.fqdn
            for site in self.sites
            if self.in_site_aliases(site, fqdn)
        ]

    def enroll_site(self, fqdn, collection):  # pylint: disable=too-complex
        # Neatness
        error = self.app.log.error
        warn = self.app.log.warning
        info = self.app.log.info

        # Skip if site path is non-existent or isn't a directory
        site_path = join(abspath(collection), fqdn)
        if not isdir(site_path):
            warn(f"{site_path} is not a directory.")
            return

        # Skip if configuration file does not exist
        site_conf_file = join(site_path, "conf", "site.yaml")
        if not exists(site_conf_file):
            warn(f"Site {fqdn} does not have a configuration file.")
            return

        # Skip if configuration file cannot be loaded
        try:
            site_conf = saturnv.conf.ConfFileYAML(site_conf_file)
        except (OSError, yaml.scanner.ScannerError) as err:
            error(f"Error reading config for {fqdn}: {err}")
            return

        # Skip if site is not configured as an active site
        if not site_conf.get("site_active"):
            info(f"{fqdn} is INACTIVE")
            return

        # Skip if site FQDN is already present
        if self.in_sites(fqdn):
            warn(f"Skipping site {fqdn} which is already present.")
            return

        # Skip if site FQDN is listed as an alias for another site
        if have_alias := self.sites_with_alias(fqdn):
            warn(f"Site {fqdn} already listed as alias for {have_alias}")
            return

        # Add the site
        this_site = FadeliskSite(site_path, site_conf, self.app)
        self.vhost.addHost(fqdn.encode(), this_site.resource)
        self.sites.append(this_site)
        info(f"Loaded site {fqdn}")

        # Add aliases
        for alias in site_conf.get("site_aliases", []):
            # Skip if already present as site
            if self.in_sites(alias):
                warn(f"Alias {alias} is already present as site")
                continue

            # Skip if listed as an alias in some other site
            if has_alias := self.sites_with_alias(alias):
                warn(
                    f"Alias {alias} already listed as alias for "
                    + ", ".join(has_alias),
                )
                continue

            # Add the alias
            this_site.aliases.append(alias)
            self.vhost.addHost(alias.encode(), this_site.resource)
            info(f"Added alias {alias} for {fqdn}")

    def getResourceFor(self, request):  # pylint: disable=invalid-name
        request.setHeader("server", self.server_header)
        return server.Site.getResourceFor(self, request)
