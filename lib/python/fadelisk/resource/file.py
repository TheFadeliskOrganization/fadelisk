from twisted.web.static import File, DirectoryLister

from .forbidden import ForbiddenResource


class FileResource(File):
    def __init__(self, path, site):
        self.path = path
        self.site = site
        super().__init__(path)

    def createSimilarFile(self, path):
        f = self.__class__(path, self.site)
        f.processors = self.processors
        f.indexNames = self.indexNames[:]
        f.childNotFound = self.childNotFound
        return f

    def directory_lister(self):
        return DirectoryLister(
            self.path,
            self.listNames(),
            self.contentTypes,
            self.contentEncodings,
            self.defaultType,
        )

    def directoryListing(self):
        for allowed_dir in self.site.conf.get("allow_directory_listing", []):
            ok_dir = self.site.rel_path("content", allowed_dir.strip("/*"))
            # This directory exactly matches an allowed directory
            if self.path == ok_dir:
                return self.directory_lister()
            # This directory was under an allowed directory, and recursive
            # listing was allowed with trailing asterisk
            if allowed_dir.endswith("*") and self.path.startswith(
                ok_dir + "/"
            ):
                return self.directory_lister()

        return ForbiddenResource(
            self.site,
            title="Directory Listing Not Allowed",
            message="Listing is not allowed for the requested directory.",
        )
