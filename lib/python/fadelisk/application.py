import os
import sys
import pprint
import saturnv

from .options import Options
from .server import FadeliskServer


class Application(saturnv.Daemon):
    """Fadelisk application

    The application container for the Fadelisk process. Handles
    loading configurations including command-line arguments,
    daemonizing the process, and dispatching command-line actions to
    start and stop the server.
    """

    default_conf = {
        "server_header": "fadelisk/1.0",
        "bind_address": "127.0.0.1",
        "listen_port": 1066,
        "process_user": "nobody",
        "daemonize": False,
        "log_level": "warning",
        "site_collections": ["/srv/www/sites"],
        "directory_index": ["index.html", "index.htm"],
        "render_extensions": [".html", ".htm"],
        "stderr_file": None,
        "extra_python_directories": ["/srv/www/lib/python"],
        "extra_template_directories": [],
    }
    """Default configuration: This built-in configuration is used if no
    configuration is found, and as a fallback for unspecified values.
    """

    def __init__(self):
        """Initializer

        This initializer starts the logger, parses the command line options,
        and loads the configuration. It also initializes the Daemon
        superclass using the loaded configuration. An initialized
        Application is ready to .run().
        """
        self.dispatch_table = None
        self.server = None

        # Set up logging with the threshold listed in the application defaults
        self.log = saturnv.Logger()
        self.log.set_level(self.default_conf["log_level"])
        self.log.stderr_on()

        # Resolve all configuration options, including command line arguments
        self.options = Options(self)
        self.args = self.options.get_args()
        self.conf = saturnv.AppConf(
            defaults=Application.default_conf,
            args=self.args,
            ignore_changes=True,
        )

        # If config is to be shown, do it and exit immediately
        if self.conf["show_config"]:
            pprint.PrettyPrinter(indent=2).pprint(self.conf.items())
            sys.exit(0)

        # Reset log level to final value from configuration
        self.log.set_level(self.conf["log_level"])

        # Initialize SaturnV's daemonizer with resolved config values
        super().__init__(stderr=self.conf["stderr_file"])

        # If specified, add extra directories to sys.path
        sys.path.extend(self.conf.get("extra_python_directories", []))

        # If specified, tokenize the list of file extensions
        # that should be processed as templates
        render_exts = self.conf["render_extensions"]
        if not isinstance(render_exts, list):
            if isinstance(render_exts, str):
                self.conf.override("render_extensions", render_exts.split(","))
            else:
                raise RuntimeError(
                    "render_extensions must be a list in the config such\n"
                    'as [".html", ".htm"], or a single extension such\n'
                    'as ".html" or a comma-separate list such as\n'
                    '".html,.htm" on the command line.'
                )

    def run(self):
        """Run the Fadelisk application"""
        # Process must be run with superuser privileges
        if os.getuid():
            sys.exit("Fadelisk must be run as superuser")

        # Legacy daemon control mechanism
        if self.conf["kill_process"] or self.conf["restart_process"]:
            # Stop background process
            lock = saturnv.Lockfile()
            try:
                lock.kill_process()
            except saturnv.LockfileProcessNotRunningError:
                sys.exit("Process is not running")
            # If stopping, simply exit
            if self.conf["kill_process"]:
                sys.exit(0)
            # If restarting, set daemon mode explicitly and fall through
            self.conf.override("daemonize", True)

        # Establish safe I/O conditions
        os.chdir("/")
        os.umask(0o027)

        # Move process to background if running in legacy daemon mode
        if self.conf["daemonize"]:
            self.daemonize()

        # Establish lock file under ultimate process ID
        lock = saturnv.Lockfile(user=self.conf["process_user"])
        try:
            lock.acquire()
        except saturnv.LockfileLockedError:
            sys.exit("Lockfile present, process already running")
        except:  # pylint: disable=bare-except
            sys.exit("Could not establish lock file")

        # Relinquish superuser privileges
        self.chuser(self.conf["process_user"])

        # Build the reactor
        self.server = FadeliskServer(self)

        # Stop output to stderr if running in legacy daemon mode
        if self.conf["daemonize"]:
            self.log.stderr_off()

        # Start the reactor (will block)
        self.server.run()

        # Remove lockfile if possible
        lock.release()
