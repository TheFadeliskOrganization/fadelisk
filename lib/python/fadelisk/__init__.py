# Bring application module into our namespace, which satisfies saturnv's
# need to discover application.Application or Application.
from .application import Application
